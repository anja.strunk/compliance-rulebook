#!/bin/sh

curdir=`dirname $0`
echo $curdir

mdArray=(
  "index.md"\
  "trust_anchors.md"\
  "participant.md"\
  "service.md"
)

echo "[TOC]" > $curdir/docs/compliance_rulebook.md

for file in ${mdArray[@]}; do
  echo $file
  cat $curdir/docs/$file >> $curdir/docs/compliance_rulebook.md
done

