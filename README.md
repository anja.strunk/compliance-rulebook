# Compliance Rulebook

Gaia-X Compliance Rulebook documents

## Document

The output is published here <https://gaia-x.gitlab.io/policy-rules-committee/compliance-rulebook/>

## Setup

1. Create a python3 virtual environment
2. `pip install -r requirements.txt`
3. `mkdocs serve -a 0.0.0.0:8000`
4. `mkdocs build`

Generated files are in `public/`