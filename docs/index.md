# Gaia-X Compliance Rulebook

This document describes the mandatory rules to conform and comply in order to be able to claim Gaia-X Compliance.

Gaia-X Compliance has the intent to create the required Trust and Transparent base layer to foster services exchange, including data.[^digitalsov]

[^digitalsov]: <https://www.europarl.europa.eu/RegData/etudes/BRIE/2020/651992/EPRS_BRI(2020)651992_EN.pdf>

The Gaia-X Compliance process is automated and versionned. It means that this document will also be versionned.

## Gaia-X Compliance scope

The compliance apply to **all** Gaia-X Self-Description files and there is a Self-Description files for **all** the entities defined as part of the Gaia-X Conceptual model described in the Gaia-X Architecture document:

This list mainly consists of:

- Participant with Consumer, Federator, Provider
- Service Offering
- Resource

## Gaia-X Self-Description

Gaia-X Self-Description files are:

- machine readable text file
- cryptographically signed file preventing tampering with its content
- using link-data to describe attributes

The format is following the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/).

## Gaia-X Compliance

The compliance is structured in 4 parts:

- serialization format and syntax.
- cryptographic signature validation.
- attribute value consistency.
- attribute veracity verification.
