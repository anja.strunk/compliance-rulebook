# Participant

For organisation, the attributes are

| Version | Attribute          | Cardinality | Signed by | Comment                                           |
|---------|--------------------|-------|-----------|---------------------------------------------------|
| 1.0     | [`company_number`](https://schema.org/taxID) | 1     | State, DV SSL | Country's registration number which identify one specific company. |
| 1.0     | [`headquarter.country`](https://schema.org/addressCountry) | 1     | State, DV SSL | Physical location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |
| 1.0     | [`legal.country`](https://schema.org/addressCountry) | 1     | State, DV SSL | Legal location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |
| 1.0     | [`lei`](https://schema.org/leiCode)   | 0..1 | gleif, DV SSL | Unique LEI number as defined by <https://www.gleif.org>. |

**Consistency rules**

- If `legal.country` is located in [EEA](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:European_Economic_Area_(EEA)), Iceland, Lichtenstein and Norway then `company_number` must be a valid ISO 6523 EUID as specified in the section 8 of the Commission Implementing [Regulation (EU) 2015/884](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32015R0884).
- `lei.headquarters.country` shall equal `headquarter.country`.
- `lei.legal.country` shall equal `legal.country`.

## Provider

A Provider aggregates with Participant.

```mermaid
classDiagram
    Participant o-- Provider
```

| Version | Attribute          | Card. | Comment                                           |
|---------|--------------------|-------|---------------------------------------------------|
| 1.0     | `gdpr`             | 0..1  | Specific attributes described in the table below. |

**Consistency rules**

- `gdpr` attributes are mandatory for `provider` legally located in EEA or providing goods or services in EEA.

### GDPR

| Version | Attribute                 | Card. | Signed by | Comment                                 |
|---------|---------------------------|-------|-----------|-----------------------------------------|
| x.x     | `to_be_defined`           | 1     | State, DV SSL, EDPB CoC | mandatory public information as defined in [GDPR art 13](https://gdpr-info.eu/art-13-gdpr/) |
| x.x     | `to_be_defined`           | 1     | State, DV SSL, EDPB CoC | mandatory public information as defined in [GDPR art 14](https://gdpr-info.eu/art-14-gdpr/) |
