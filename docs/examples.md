# Examples

## Simple Fortune teller

Example of a simple API endpoint returning a fortune from the BSD packet [fortune](https://en.wikipedia.org/wiki/Fortune_(Unix)). 

For the same service offering, 3 examples of service offering are detailled with 3 different transparency level:  
trust_index(**serviceoffering1_claim1**) < trust_index(**serviceoffering1_claim2**) < trust_index(**serviceoffering1_claim3**)

**serviceoffering1_claim1**

```mermaid
flowchart TD
    participant1_claim1 --> participant1_claim2

    serviceoffering1_claim1 --> participant1_claim1
```

```yaml
name: Fortune teller
description: API to randomly return a fortune
service_provider: url(participant1_claim1)
terms_and_conditions:
  - https://some.url.for.terms.and.condition.example.com
```

**serviceoffering1_claim2**

```mermaid
flowchart TD
    participant1_claim1 --> participant1_claim2

    serviceoffering1_claim2 --> participant1_claim1
    serviceoffering1_claim2 --> resource2_claim1

    resource2_claim1 --> participant1_claim1
```

```yaml
name: Fortune teller
description: API to randomly return a fortune
service_provider: url(participant1_claim1)
resource:
  - url(resource2_claim1)
terms_and_conditions:
  - https://some.url.for.terms.and.condition.example.com
```

**serviceoffering1_claim3**

```mermaid
flowchart TD
    participant1_claim1 --> participant1_claim2

    serviceoffering1_claim3 --> participant1_claim1
    serviceoffering1_claim3 --> resource1_claim1
    serviceoffering1_claim3 --> resource2_claim1
    serviceoffering1_claim3 --> resource3_claim1

    resource2_claim1 --> participant1_claim1

    resource3_claim1 --> participant1_claim1
    resource3_claim1 --> participant2_claim1
```

```yaml
name: Fortune teller
description: API to randomly return a fortune
service_provider: url(participant1_claim1)
resource:
  - url(resource1_claim1)
  - url(resource2_claim1)
  - url(resource3_claim1)
terms_and_conditions:
  - https://some.url.for.terms.and.condition.example.com
policies:
  - type: opa
    content: |-
      package fortune
      allow = true {
        input.method = "GET"
      }
```

**participant1_claim1**

```yaml
company_number: FR5910.899103361
headquarter:
  country: FR
legal:
  country: FR
lei: url(participant1_claim2)
```

**participant1_claim2**

```yaml
lei: 9695009HSLO8O4OSX011
```

**participant2_claim1**

```yaml
company_number: FR5910.424761419
headquarter:
  country: FR
legal:
  country: FR
```

**resource1_claim1**
```yaml
name: fortune dataset
copyright_owner:
  - name: The Regents of the University of California
    company_number:
      duns: 003985512
    headquarter:
      state: CA
      country: USA
    legal:
      state: CA
      country: USA
license:
  - BSD-3
  - https://metadata.ftp-master.debian.org/changelogs//main/f/fortune-mod/fortune-mod_1.99.1-7.1_copyright
```

**resource2_claim1**
```yaml
name: api software
copyright_owner:
  - url(participant1_claim1)
license:
  - EPL-2.0
```

**resource3_claim1**
```yaml
name: datacenter
tenant_owner: url(participant1_claim1)
resource_maintainer: url(participant2_claim1)
location:
  - country: FR
```
