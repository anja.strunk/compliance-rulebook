# Service

Gaia-X will define a 3-level taxonomy for services. This taxonomy is being worked on by the Gaia-X Working Groups and will be published in a separate document.

```mermaid
classDiagram
    ServiceOffering o-- Resource
    Resource o-- Resource
    Resource <|-- PhysicalResource
    Resource <|-- VirtualResource
``` 

## Service offering

This is the generic format for all service offerings

| Version | Attribute               | Card. | Signed by     | Comment                                         |
|---------|-------------------------|-------|---------------|-------------------------------------------------|
| 1.0     | `service_provider`      | 1     | State, DV SSL | a `provider`'s URL which provide the service    |
| 1.0     | `resource[]`            | 0..*  | State, DV SSL | a list of `resource` used by the service        |
| 1.1     | `terms_and_conditions[]`| 1..*  | State, DV SSL | a list of `terms_and_conditions`  |
| 1.1     | `policies[]`            | 0..*  | State, DV SSL | a list of `policy` expressed using DSL(Rego/Lucon/MyShare/ODRL/...) |

## Resource

A resource aggregates with Service Offering.

| Version | Attribute               | Card. | Signed by     | Comment                                         |
|---------|-------------------------|-------|---------------|-------------------------------------------------|
| 1.0     | `resource[]`            | 0..*  | State, DV SSL | a list of `resource` used by the resource       |

### Physical Resource

A Physical Resouce inherits from a Resource.

| Version | Attribute              | Card. | Signed by     | Comment                                     |
|---------|------------------------|-------|---------------|---------------------------------------------|
| 1.0     | `tenant_owner[]`       | 1..*  | State, DV SSL | a list of `participant` owning the resource |
| 1.0     | `resource_maintainer[]`| 1..*  | State, DV SSL | a list of `participant` maintaining the resource |
| 1.0     | `location[].country`   | 1..*  | State, DV SSL | a list of physical location in ISO 3166-1 alpha2, alpha-3 or numeric format. |

### Virtual Resource

A Virtual Resource inherits from a Resource.  
A Virtual resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file.

| Version | Attribute            | Card. | Signed by     | Comment                                     |
|---------|----------------------|-------|---------------|---------------------------------------------|
| 1.0     | `copyright_owner[]`  | 1..*  | State, DV SSL | a list of copyright owner                 |
| 1.0     | `license[]`          | 1..*  | State, DV SSL | a list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) license identifiers or URL to license document |
